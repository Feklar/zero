#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os

def main():
    """
    Counts and prints lines in file.
    
    Read a file name from first command-line argument.
    Counts total, effective, comments and empty lines.
    Not effective lines are lines that contain non-printable symbols only
    and python comments (lines that begin witn #)
    
    """
    
    #sys.argv.append(sys.argv[0]) #for testing
    if len(sys.argv) == 1:
        print(main.__doc__)
        return
    if sys.argv[1] == '-h':
        print(main.__doc__)
        return
    
    file = open(sys.argv[1], 'r')
    total_lines_count = 0
    comments_lines_count = 0
    empty_lines_count = 0
    effective_lines_count = 0
    for line in file:
        total_lines_count = total_lines_count + 1
        is_comment = line.strip().startswith('#')
        is_empty = line.isspace()
        if is_comment:
            comments_lines_count+=1
        if is_empty:
            empty_lines_count+=1
        if not is_comment and not is_empty:
            effective_lines_count = effective_lines_count + 1
            
    print('Total lines:', total_lines_count)
    print('Effective lines:', effective_lines_count)
    print('Comments lines:', comments_lines_count)
    print('Empty lines:', empty_lines_count)
    
if __name__ == "__main__":
    main()
